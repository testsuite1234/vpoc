<html>
<head>
    <title>Travel List to show to Dileep</title>
</head>

<body>
    <h1>Travel Bucket List - PoC - Demo given to Andy and team</h1>
    <h2>Places Rajesh wants to Visit</h2>
    <ul>
      @foreach ($togo as $newplace)
        <li>{{ $newplace->name }}</li>
      @endforeach
    </ul>

    <h2>Places I've Already Been To</h2>
    <ul>
          @foreach ($visited as $place)
                <li>{{ $place->name }}</li>
          @endforeach
    </ul>
</body>
</html>
